var socket = io(socket_host);
vex.defaultOptions.className = 'vex-theme-default';

function appMessage(message, callback) {
    var opts = {
        message: message,
        buttons: [vex.dialog.buttons.YES]
    };

    if (callback) {
        opts.callback = callback;
    }

    vex.dialog.open(opts);
}

function roomResponse(data) {
    if (data.error)
        return appMessage(data.error, joinRoom);

    document.getElementById('msg-body').focus();
    console.log(data.success);
}

function sendMessage(e) {
    e.preventDefault();
    var text = document.getElementById('msg-body');

    var message = {
        text: text.value,
    };

    socket.emit('createMessage', message, function (resp) {
        if (resp.noUser)
            return appMessage(resp.error, joinRoom);

        console.log(message);
        text.value = '';
        document.getElementById('msg-body').focus();
    });
}

function leaveRoom() {
    socket.emit('leaveroom');
}

function renderMessage(response) {
    var messageData = {
        from: response.from,
        text: response.text,
        opts: response.opts
    };

    var template = document.getElementById('message-template').innerHTML;
    var message = Mustache.render(template, messageData);

    var list = document.getElementById('messages');
    list.innerHTML += message;

    scrollToBottom(list);
}

function scrollToBottom(list) {
    var listTotalHeight = list.scrollHeight;
    var listVisiblePortion = list.clientHeight;

    if (listTotalHeight <= listVisiblePortion)
        return;

    var addedItem = list.lastElementChild;
    var lastItem = addedItem.previousElementSibling;

    var pixelsFromTop = list.scrollTop;
    var addedItemHeight = addedItem.clientHeight;
    var lastItemHeight = lastItem.clientHeight;

    var total = listVisiblePortion + pixelsFromTop + addedItemHeight + lastItemHeight;

    if (total >= listTotalHeight)
        list.scrollTop = listTotalHeight;
}

function updateUserList(users) {
    var list = document.createElement('ol');

    users.forEach(function (user) {
        var li = document.createElement('li');
        li.innerText = user;
        list.appendChild(li);
    });
}

document.getElementById('message-form').addEventListener('submit', sendMessage);
document.getElementById('leave-room').addEventListener('click', leaveRoom);

socket.on('connect', function () {
    console.log('Connected to server');
});

socket.on('join-room-user', function () {
    console.log('join room user');
});

socket.on('join-room-BC', function () {
    console.log('join room BC');
});

socket.on('newMessage', renderMessage);
socket.on('welcome', renderMessage);
socket.on('AdminBC', renderMessage);
socket.on('updateUserList', updateUserList);
socket.on('newLocationUrl', renderMessage);

socket.on('disconnect', function () {
    console.log('Disconnected from server');
});

socket.on('leave-room-user', function () {
    console.log('leave room user');
});

socket.on('leave-room-BC', function () {
    console.log('leave room BC');
});
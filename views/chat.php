<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../public/css/styles.css">
    <link rel="stylesheet" href="../public/css/vex.css">
    <link rel="stylesheet" href="../public/css/vex-theme-default.css">
    <script type="text/javascript">
        var socket_host = "<?= $socket_host ?>";
    </script>
    <title>WEBSOCKET</title>
</head>

<body class="chat">

    <div class="chat__main">
        <ol id="messages" class="chat__messages"></ol>

        <div class="chat__footer">
            <form id="message-form">
                <input type="text" name="message" id="msg-body" placeholder="Message" autocomplete="off">
                <button id="msg-send">Send</button>
            </form>
            <button id="leave-room">Leave Room</button>
        </div>

    </div>

</body>

<script id="message-template" type="x-tmpl-mustache">
    <li class="message">
        <div class="message__title">
            <h4>{{from}}</h4>
            <!-- <span>{{createdAt}}</span> -->
        </div>
        <div class="message__body">
            <p>
                {{#opts.plain}}
                    {{ text }}
                {{/opts.plain}}

                {{#opts.anchor}}
                    <a href="{{opts.href}}" target="_blank">{{text}}</a>
                {{/opts.anchor}}
            </p>
        </div>
    </li>
</script>

<script src="../public/js/vendors/mustache.min.js"></script>
<script src="../public/js/vendors/vex.combined.min.js"></script>
<script src="../public/js/vendors/socket.io.js"></script>
<script src="../public/js/polyfill.js"></script>
<script src="../public/js/chat.js"></script>
<script>

</script>

</html>
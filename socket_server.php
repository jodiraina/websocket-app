<?php
require 'vendor/autoload.php';

use Workerman\Worker;
use PHPSocketIO\SocketIO;
use Utils\Message;
use Utils\UserList;

use function Utils\Helpers\isRealString;
use phpDocumentor\Reflection\Types\Null_;

$io = new SocketIO(2020);
$io->userList = new UserList;

$io->on('connection', function ($socket) use ($io) {
    echo "New client connected\n";
    $socket->emit('connect');

    $data['room'] = "room" . $socket->id;
    $data['name'] = "user";
    $data['roomBC'] = "roomBC";
    $user = $io->userList->addUser($socket->id, $data['name'], $data['room'], $data['roomBC']);

    //Join a room
    $socket->join($user->roomBC);
    $socket->emit('join-room-BC');

    $io->in($user->roomBC)->emit('AdminBC', Message::fromAdmin($user->name, 'BC'));

    $socket->on('createMessage', function ($data, $callback) use ($socket, $io) {
        $user = $io->userList->getUser($socket->id);
        $socket->emit('join-room-user');
        $socket->join($user->room);

        if (!isRealString($data['text'])) {
            $callback([
                'error' => 'Please send a valid message'
            ]);
            return;
        }

        $io->in($user->room)
            ->emit('newMessage', Message::create($user->name, $data['text']));

        $callback([
            'success' => 'Message send succesfully'
        ]);
    });

    $socket->on('leaveroom', function () use ($socket, $io) {
        $user = $io->userList->getUser($socket->id);

        if ($user === NULL)
            return;

        if (!$socket->join($user->room)) {
            echo "Can't leave room\n";
        } else {
            $socket->leave($user->room);
            $socket->emit('leave-room-user');
            echo "leave room\n";
            var_dump($user);
        }
    });

    $socket->on('disconnect', function () use ($socket, $io) {
        echo "Client disconnected\n";

        $user = $io->userList->getUser($socket->id);

        if ($user === NULL)
            return;

        $socket->leave($user->roomBC);
        $socket->emit('leave-room-BC');
        $io->userList->removeUser($user->id);
    });
});

Worker::runAll();
